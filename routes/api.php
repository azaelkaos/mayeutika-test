<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('forgotpassword', [UserController::class, 'forgotPassword']);
Route::post('resetpassword', [UserController::class, 'resetPassword']);
Route::post('login', [UserController::class, 'login']);
Route::post('create', [UserController::class, 'create']);
Route::group( ['middleware' => ["auth:sanctum"]], function(){
	Route::get('logout', [UserController::class, 'logout']);
	Route::put('update', [UserController::class, 'update']);
	Route::get('list', [UserController::class, 'list']);
	Route::get('description', [UserController::class, 'description']);
	Route::put('changestatus', [UserController::class, 'changeStatus']);
	Route::put('changepassword', [UserController::class, 'changePassword']);
});
