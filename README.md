# mayeutika test

Test for mayeutika

## Getting started

This project is the exam to be part of the backend team.

The code was created on PHP 8.0 with laravel. 

### important

you have to have composer installed

you have to have mysql to database


## Installation

#### Important
you have to import the test db at your local environment or test server before to test it

```
mysql -u YOURUSER -p < mayeutika_test.sql
```


### Steps
I give you 2 ways to do that

### Option 1: Unsing artisan (from laravel)

Run command artisan

```
git clone https://gitlab.com/azaelkaos/mayeutika-test.git
cd mayeutika-test
```
Create a .env file based on .env.example

In my case, this was my .env file:
```
APP_NAME=Laravel
APP_ENV=local
APP_KEY=base64:zOz23TlS4D5NFNxeeOYq/ZcW76c8NOTTlVrG7l9xZWI=
APP_DEBUG=true
APP_URL=http://mayeutica.test

LOG_CHANNEL=stack
LOG_DEPRECATIONS_CHANNEL=null
LOG_LEVEL=debug

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=mayeutika_test
DB_USERNAME=homestead
DB_PASSWORD=secret

BROADCAST_DRIVER=log
CACHE_DRIVER=file
FILESYSTEM_DRIVER=local
QUEUE_CONNECTION=sync
SESSION_DRIVER=file
SESSION_LIFETIME=120

MEMCACHED_HOST=127.0.0.1

REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379

MAIL_MAILER=smtp
MAIL_HOST=localhost
MAIL_PORT=1025
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null
MAIL_FROM_ADDRESS=mail@net.com
MAIL_FROM_NAME="${APP_NAME}"

AWS_ACCESS_KEY_ID=
AWS_SECRET_ACCESS_KEY=
AWS_DEFAULT_REGION=us-east-1
AWS_BUCKET=
AWS_USE_PATH_STYLE_ENDPOINT=false

PUSHER_APP_ID=
PUSHER_APP_KEY=
PUSHER_APP_SECRET=
PUSHER_APP_CLUSTER=mt1

MIX_PUSHER_APP_KEY="${PUSHER_APP_KEY}"
MIX_PUSHER_APP_CLUSTER="${PUSHER_APP_CLUSTER}"

```
Excecute
```
npm install
php artisan migrate
php artisan serve
```

The command prompt show you a local ip to show your server



### Option 2: Unsing an apache server for test environment

To set your environment, go to your www route folder

For example:
```
cd /var/www
```
excecute the commands:

```
git clone https://gitlab.com/azaelkaos/mayeutika-test.git
cd mayeutika-test
```
Create a .env file based on .env.example
(read a few lines up, at option 1 .env)

Excecute:
```
npm install
```

## How to use it

Here is an API that response all requiremenst for the test

so, in your requester clientes, as postman, you can execute the followin request:

remember that you have to have your local ip or domain to check the response:

my server was: http://mayeutika.test

#### Create:
```
curl --location --request POST 'http://mayeutika.test/api/create' \
--header 'Accept: application/json' \
--header 'Content-Type: application/json' \
--data-raw '{
    "name": "first",
    "email": "first@mail.com",
    "password": "123456789"
}'
```

Body:
```
"name": required
"email": required | email
"password": required
```

Success Response
```
{
    "status": 1,
    "message": "Successfully created"
}
```


#### Login:
```
curl --location --request POST 'http://mayeutika.test/api/login' \
--header 'Accept: application/json' \
--header 'Content-Type: application/json' \
--data-raw '{
    "email": "first@mail.com",
    "password": "123456789"
}'
```

Body:
```
"email": required | email
"password": required
```

Success Response
```
{
    "status": 1,
    "message": "Login success!",
    "access_token": "6|MCeU24EzyFMkqcHSn9a5kQN9JvZVrHCB8QZvljKW"
}
```

#### Logout:
```
curl --location --request GET 'http://mayeutika.test/api/logout' \
--header 'Accept: application/json' \
--header 'Content-Type: application/json' \
--header 'Authorization: Bearer 2|P2KUXkDigAfGZpYuPvLpBCd7KHXci2acCDE7fQXy' \
```

Success Response
```
{
    "status": 1,
    "message": "Logout"
}
```


#### Update:
```
curl --location --request PUT 'http://mayeutika.test/api/update' \
--header 'Accept: application/json' \
--header 'Content-Type: application/json' \
--header 'Authorization: Bearer 2|P2KUXkDigAfGZpYuPvLpBCd7KHXci2acCDE7fQXy' \
--data-raw '{
    "email":"second@mail.com",
    "name":"second"
}'
```
Body:
```
"email": optional | email
"password": optional
```
Success Response
```
{
    "status": 1,
    "message": "Successfully updated"
}
```

#### List:
```
curl --location --request GET 'http://mayeutika.test/api/list' \
--header 'Accept: application/json' \
--header 'Content-Type: application/json' \
--header 'Authorization: Bearer 2|P2KUXkDigAfGZpYuPvLpBCd7KHXci2acCDE7fQXy' \
--data-raw ''
```

Success Response
```
{
    "status": 1,
    "message": "",
    "user": [
        {
            "id": 2,
            "name": "second",
            "email": "second@gmail.com",
            "status": "active",
            "created_at": "2022-05-28T18:01:33.000000Z",
            "updated_at": "2022-05-28T18:16:17.000000Z"
        },
        {
            "id": 3,
            "name": "three",
            "email": "three@test.com",
            "status": "active",
            "created_at": "2022-05-28T18:01:47.000000Z",
            "updated_at": "2022-05-28T18:01:47.000000Z"
        }
    ]
}
```

#### Description:
```
curl --location --request GET 'http://mayeutika.test/api/description?id={ID}' \
--header 'Accept: application/json' \
--header 'Content-Type: application/json' \
--header 'Authorization: Bearer 2|P2KUXkDigAfGZpYuPvLpBCd7KHXci2acCDE7fQXy' \
```
Url query:
```
"id": required|numeric
```

Success Response
```
{
    "status": 1,
    "message": "",
    "user": {
        "id": 2,
        "name": "second",
        "email": "second@gmail.com",
        "status": "active",
        "created_at": "2022-05-28T18:01:33.000000Z",
        "updated_at": "2022-05-28T18:16:17.000000Z"
    }
}
```

#### Change status:
```
curl --location --request PUT 'http://mayeutika.test/api/changestatus' \
--header 'Accept: application/json' \
--header 'Content-Type: application/json' \
--header 'Authorization: Bearer 2|P2KUXkDigAfGZpYuPvLpBCd7KHXci2acCDE7fQXy' \
--data-raw '{
    "status":"suspended"
}'
```

Body:
```
"status": required | in:active|suspended
```

Success Response
```
{
    "status": 1,
    "message": "Successfully updated"
}
```

#### Change password:
```
curl --location --request PUT 'http://mayeutika.test/api/changepassword' \
--header 'Accept: application/json' \
--header 'Content-Type: application/json' \
--header 'Authorization: Bearer 4|pzifHNU8fKzJLmXuUOjlOo4xJ0DNIfJsB473nxRI' \
--data-raw '{
    "password":"987654321"
}'
```
Body:
```
"password": required
```

Success Response
```
{
    "status": 1,
    "message": "Successfully updated"
}
```

#### Forgot password:
```
curl --location --request POST 'http://mayeutika.test/api/forgotpassword' \
--header 'Accept: application/json' \
--header 'Content-Type: application/json' \
--data-raw '{
    "email":"first@mail.com"
}'
```
Body:
```
"email": required| email
```

Success Response
```
{
    "message": "We have emailed your password reset link!",
    "status": 1
}
# it will send an email with the token that we need in the resetpassword request
```

#### Reset password:
```
curl --location --request POST 'http://mayeutika.test/api/resetpassword' \
--header 'Accept: application/json' \
--header 'Content-Type: application/json' \
--data-raw '{
    "token":"f492cc0674520f5f7d346f2f435f8c08333030d47b6a3b9c81a522beda0875c6",
"email":"first@mail.com",
"password":"195137535"
}'
```

Body:
```
{
    "token": required|token,
"email":required|email,
"password":required
}
```
Response:
```
{
    "status": 1,
    "message": "Password reset successfully"
}
```