<?php

namespace App\Domain\User\Repository;

use App\Domain\User\Models\User;
use Illuminate\Support\Collection;

interface UserRepositoryInterface
{
    /**
     * @param $id
     * @return mixed
     */
    public function find($id): User;

    /**
     * @return mixed
     */
    public function findAll(): Collection;
}