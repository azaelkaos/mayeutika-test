<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Password;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Validation\Rules\Password as RulesPassword;
use Illuminate\Auth\Events\Verified;
class UserController extends Controller
{
	public function create(Request $request) {
		$request->validate([
			'name'	=>	'required',
            'email'			=>	'required|email|unique:users',
            'password'		=>	'required'
        ]);
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
		$user->save();
		return response()->json([
			"status" => 1,
			"message" => "Successfully created"
		],201);
    }

    public function login(Request $request) {
		$request->validate([
            "email" => "required|email",
            "password" => "required"
        ]);
		if(!Auth::attempt($request->only('email','password'))){
			return response()->json([
				"status" => 0,
				"message" => "The user does not exist or the password is incorrect.",
			], 401);
		}
        $user = User::where("email", "=", $request->email)->first();
		$token = $user->createToken("auth_token")->plainTextToken;
		return response()->json([
			"status" => 1,
			"message" => "Login success!",
			"access_token" => $token
		], 200);
    }
	
    public function logout() {
        auth()->user()->tokens()->delete();
        return response()->json([
            "status" => 1,
            "message" => "Logout",
        ],200); 
    }

	public function update(Request $request){
		$user = auth()->user();
		$update=[];
		if($request->email!=$user->email){
			$request->validate([
				'email'	=>	'required|email|unique:users',
			]);
			$user->email = $request->email;
			$update['email']=$request->email;
		}
		if($request->name){
			$update['name']=$request->name;
		}
		User::where('id',$user->id)->update($update);
		return response()->json([
			"status" => 1,
			"message" => "Successfully updated"
		],201);
	}

	public function list(){
		return response()->json([
			"status" => 1,
			"message" => "",
			"user" => User::where('status','active')->get()
		],200);
	}

	public function description(Request $request){
		$request->validate([
			'id'=>'required|numeric'
        ]);
		$user = User::where("id", "=", $request->id)->get()->first();
		return response()->json([
			"status" => 1,
			"message" => "",
			"user" => $user
		],200);
	}

	public function changeStatus(Request $request){
		$user = auth()->user();
		$request->validate([
			'status'=>'required'
        ]);
		User::where('id',$user->id)->update(['status'=>$request->status]);
		return response()->json([
			"status" => 1,
			"message" => "Successfully updated"
		],201);
	}

	public function changePassword(Request $request){
		$user = auth()->user();
		$request->validate([
			'password'=>'required'
        ]);
		User::where('id',$user->id)->update(['password'=>Hash::make($request->password)]);
		return response()->json([
			"status" => 1,
			"message" => "Successfully updated"
		],201);
	}

	public function forgotPassword(Request $request){
        $request->validate([
            'email' => 'required|email',
        ]);

        $status = Password::sendResetLink(
            $request->only('email')
        );

        if ($status == Password::RESET_LINK_SENT) {
            return [
				'message'=> __($status),
                'status' => 1
            ];
        }else{
			return response()->json([
				'status' => 0,
				'message'=> __($status)
			], 500);  
		}
    }

    public function resetPassword(Request $request){
        $request->validate([
            'token' => 'required',
            'email' => 'required|email',
            'password' => ['required'],
        ]);

        $status = Password::reset(
            $request->only('email', 'password','token'),
            function ($user) use ($request) {
                $user->forceFill([
                    'password' => Hash::make($request->password)
                ])->save();
                $user->tokens()->delete();
                event(new PasswordReset($user));
            }
        );

        if ($status == Password::PASSWORD_RESET) {

			return response()->json([
                'status' => 1,
                'message'=> 'Password reset successfully'
            ], 200);  
        }

		return response()->json([
			'status' => 0,
            'message'=> __($status)
		], 500);  
    }
}